// jQuery
window.$ = window.jQuery = require('jquery');

// Popper JS
window.Popper = require('popper.js').default;

// Bootstrap
require('bootstrap');